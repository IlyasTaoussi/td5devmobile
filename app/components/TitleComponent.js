import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

class TitleComponent extends React.Component {
    render(){
        return (
            <View>
                <Text style={styles.text}>Show Random User</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        color: "white",
        fontSize: 20,
        textDecorationLine: "underline",
        textAlign: 'center',
        marginBottom: 20,
        fontWeight: 'bold'
    },
});

export default TitleComponent
