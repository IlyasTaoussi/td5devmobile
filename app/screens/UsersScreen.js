import React from 'react';
import { StyleSheet, Text, View , Button } from 'react-native';
import TitleComponent from "../components/TitleComponent"
import UsersComponent from "../components/UsersComponent";

class UsersScreen extends React.Component {
    constructor(props){
        super(props)
        
    }

    goToUsersDetailsScreen(params){
        this.props.navigation.navigate('Details', params)
    }

    render(){
        return (
            <View style={styles.container}>
                <TitleComponent />
                <UsersComponent goToUsersDetailsScreen ={this.goToUsersDetailsScreen.bind(this)} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
      backgroundColor: "blue",
      color: "white",
      fontSize: 20
    },
    container: {
        flex: 1,
        backgroundColor: 'grey'
    },
});

export default UsersScreen
