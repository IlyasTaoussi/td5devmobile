import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

class UserDetailScreen extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        
    }

    render(){
        return (
            <View>
                <Text style={styles.text}>{this.props.route.params.name}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
      backgroundColor: "blue",
      color: "white",
      fontSize: 20
    },
});

export default UserDetailScreen
