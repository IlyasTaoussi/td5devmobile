import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import UsersScreen from '../screens/UsersScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import UserDetailScreen from '../screens/UserDetailScreen';

const Stack = createStackNavigator();

class Navigator extends React.Component {
    constructor(props){
        super(props)
    }

    render(){
        return (
            <NavigationContainer>
                <Stack.Navigator initialRouteName="Home">
                    <Stack.Screen name="Home" component={UsersScreen} />
                    <Stack.Screen name="Details" component={UserDetailScreen} />
                </Stack.Navigator>
            </NavigationContainer>
        )
    }
}


  

  
  
const styles = StyleSheet.create({
    text: {
      backgroundColor: "blue",
      color: "white",
      fontSize: 20
    },
});

export default Navigator
